import React from 'react';
import './App.css';
import { Link,NavLink } from 'react-router-dom';
const Nav = () =>{
    const navStyle = {
        color: 'white'
    }
    return (
      <nav>
          <NavLink to='/' style={{color: 'white'}}><h3>Navigation</h3></NavLink>
          <ul className="nav-links">
              <NavLink style={navStyle} activeStyle={{color: 'green'}} to='/about'><li>About</li></NavLink>
             <NavLink style={navStyle} activeStyle={{color: 'green'}} to='/shop'><li>Shop</li></NavLink> 
          </ul>
      </nav>
    );
}

export default Nav;